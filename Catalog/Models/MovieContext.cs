﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Catalog.Models
{
    public class MovieContext : DbContext
    {
        public MovieContext() : base("DefaultConnection") { }
        public DbSet<Movie> Movies { get; set; }
    }
}